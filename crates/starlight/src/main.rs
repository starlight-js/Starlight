use starlight::{
    bytecode::opcodes::Op,
    runtime::{
        arguments::Arguments,
        function::JsVMFunction,
        object::{object_size_with_tag, JsObject},
        value::JsValue,
    },
    vm::{Options, VirtualMachineRef},
};
use starlight::{bytecode::ByteCodeBuilder, vm::VirtualMachine};
use structopt::StructOpt;

fn main() {
    println!(
        "{}",
        object_size_with_tag(starlight::runtime::object::ObjectTag::Ordinary)
    );
    let mut vm = VirtualMachine::new(Options::from_args());
    {
        let ctx = vm.space().new_local_context();
        let name = vm.intern("foo");
        let px = vm.intern("x");
        let mut builder = ctx.new_local(ByteCodeBuilder::new(&mut vm, name, &[px], false));
        let name = builder.get_sym(px);
        builder.emit(Op::OP_GET_VAR, &[name], true);

        builder.emit(Op::OP_PUSH_INT, &[3], false);

        builder.emit(Op::OP_ADD, &[], false);
        builder.emit(Op::OP_RET, &[], false);
        let bcode = builder.finish();
        let env = JsObject::new_empty(&mut vm);
        let mut f = JsVMFunction::new(&mut vm, bcode, env);
        let mut args = ctx.new_local(Arguments::new(&mut vm, JsValue::undefined(), 1));
        args[0] = JsValue::new(3);
        if let Ok(val) = f.as_function_mut().call(&mut vm, &mut args) {
            assert!(val.is_int32());
            println!("{}", val.as_int32());
        }
        let mut args = ctx.new_local(Arguments::new(&mut vm, JsValue::undefined(), 1));
        args[0] = JsValue::new(4.6);
        match f.as_function_mut().call(&mut vm, &mut args) {
            Ok(val) => {
                assert!(val.is_double());
                println!("{}", val.as_double());
            }
            Err(e) => {
                println!(
                    "{}",
                    e.to_string(&mut vm)
                        .unwrap_or_else(|_| "can't get error".to_string())
                );
            }
        }
        //drop(ctx);
    }
    vm.space().gc();
    VirtualMachineRef::dispose(vm);
}
