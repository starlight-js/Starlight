use std::{collections::HashMap, ptr::null_mut};

#[cfg(feature = "debug-snapshots")]
use serde::ser::SerializeStruct;

use crate::{
    heap::{
        cell::{Cell, Gc, Trace, Tracer},
        Allocator,
    },
    runtime::{string::JsString, structure::Structure, symbol::Symbol, value::JsValue},
    vm::VirtualMachine,
};

pub mod opcodes;
use starlight_derive::Trace;
#[derive(Trace)]
pub struct ByteCode {
    #[unsafe_ignore_trace]
    pub name: Symbol,
    #[unsafe_ignore_trace]
    pub code: Vec<u8>,
    #[unsafe_ignore_trace]
    pub code_start: *mut u8,
    pub codes: Vec<Gc<ByteCode>>,
    pub feedback: Vec<TypeFeedBack>,
    pub literals: Vec<JsValue>,
    #[unsafe_ignore_trace]
    pub literals_start: *mut JsValue,
    #[unsafe_ignore_trace]
    pub names: Vec<Symbol>,
    #[unsafe_ignore_trace]
    pub params: Vec<Symbol>,
    #[unsafe_ignore_trace]
    pub strict: bool,
}

impl ByteCode {
    pub fn new(vm: &mut VirtualMachine, name: Symbol, params: &[Symbol], strict: bool) -> Gc<Self> {
        vm.allocate(Self {
            name,
            code: vec![],
            code_start: null_mut(),
            codes: vec![],
            feedback: vec![],
            literals: vec![],
            literals_start: null_mut(),
            names: vec![],
            params: params.to_vec(),
            strict,
        })
    }
}

impl Cell for ByteCode {}
#[cfg(feature = "debug-snapshots")]
impl serde::Serialize for ByteCode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut x = serializer.serialize_struct("ByteCode", 2)?;
        x.serialize_field("code", &self.code)?;
        x.serialize_field("is_strict", self.strict)?;
        x.end()
    }
}
pub enum TypeFeedBack {
    Generic,
    Structure(
        Gc<Structure>,
        u32, /* field offset */
        u32, /* number of ICs happened */
    ),
    None,
    X,
}
unsafe impl Trace for TypeFeedBack {
    fn trace(&self, tracer: &mut dyn Tracer) {
        match self {
            Self::Structure(ref x, _, _) => x.trace(tracer),
            _ => (),
        }
    }
}

pub struct ByteCodeBuilder {
    pub code: Gc<ByteCode>,
    pub name_map: HashMap<Symbol, u32>,
    pub val_map: HashMap<Val, u32>,
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum Val {
    Float(u64),
    Str(String),
}
impl ByteCodeBuilder {
    pub fn finish(&mut self) -> Gc<ByteCode> {
        self.code.code_start = &mut self.code.code[0];

        self.code
    }
    pub fn new(vm: &mut VirtualMachine, name: Symbol, params: &[Symbol], strict: bool) -> Self {
        Self {
            code: ByteCode::new(vm, name, params, strict),
            val_map: Default::default(),
            name_map: Default::default(),
        }
    }
    pub fn get_val(&mut self, vm: &mut VirtualMachine, val: Val) -> u32 {
        if let Some(ix) = self.val_map.get(&val) {
            return *ix;
        }

        let val_ = match val.clone() {
            Val::Float(x) => JsValue::new(f64::from_bits(x)),
            Val::Str(x) => JsValue::new(JsString::new(vm, x)),
        };
        let ix = self.code.literals.len();
        self.code.literals.push(val_);
        self.val_map.insert(val, ix as _);
        ix as _
    }
    pub fn get_sym(&mut self, name: Symbol) -> u32 {
        if let Some(ix) = self.name_map.get(&name) {
            return *ix;
        }
        let ix = self.code.names.len();
        self.code.names.push(name);
        self.name_map.insert(name, ix as _);
        ix as _
    }
    pub fn emit(&mut self, op: opcodes::Op, operands: &[u32], add_feedback: bool) {
        self.code.code.push(op as u8);
        for operand in operands.iter() {
            for x in operand.to_le_bytes().iter() {
                self.code.code.push(*x);
            }
        }
        if add_feedback {
            let f_ix = self.code.feedback.len() as u32;
            self.code.feedback.push(TypeFeedBack::None);
            for x in f_ix.to_le_bytes().iter() {
                self.code.code.push(*x);
            }
        }
    }
}
unsafe impl Trace for ByteCodeBuilder {
    fn trace(&self, tracer: &mut dyn Tracer) {
        self.code.trace(tracer);
    }
}
